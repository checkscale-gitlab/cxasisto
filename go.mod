module gitlab.com/thsp/cxasisto

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.1.0
	github.com/gocolly/colly v0.0.0-20180106205327-dface0bcda09
)

require (
	github.com/andybalholm/cascadia v0.0.0-20161224141413-349dd0209470 // indirect
	github.com/gobwas/glob v0.2.2 // indirect
	github.com/golang/protobuf v0.0.0-20160106020635-2402d76f3d41 // indirect
	github.com/kennygrant/sanitize v1.2.3 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/temoto/robotstxt v0.0.0-20170603013557-9e4646fa7053 // indirect
	golang.org/x/net v0.0.0-20160126033523-f315505cf334 // indirect
	golang.org/x/text v0.0.0-20170814122439-e56139fd9c5b // indirect
	google.golang.org/appengine v1.0.0 // indirect
)
